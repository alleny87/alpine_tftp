Continer based on alpine, used to run a TFTP server

docker run -v volume:/opt/tftpboot -p 69:69udp alpine_tftpd:latest

Requires open port: 69/UDP
A volume can be mapped in to: /opt/tftboot

Utilizes supervisord to run in.tftpd and busybox syslog
